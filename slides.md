---
title: Crypto 100
subtitle: (101 - 1)
theme: AnnArbor
author: Philippe Pépos Petitclerc
institute: SecUQAM
date: 2022-07-21
---

# Intro

1. Données binaires et codage
2. Ou exclusif: théorie
3. Ou exclusif: exemple pratique
4. Exercices
5. Bonus

# Codage binaire

- But: Représenter des octets arbitraires sous forme ASCII (texte)

Des exemples:

 - Hexadécimal (`/([0-9a-a]{2})*/i`)
   - `426f6e6a6f75722c206c65206d6f6e646521`
 - Base64 (`[0-9a-zA-Z+/]` et `=` de padding)
   - `RkxBRy1pbF9mYXV0X3JlY29ubmFpdHJlX1JreEI=`

On veut manipuler les octets représentés, pas ceux que l'on lit.

# Ou Exclusif (oubedon)

XOR, EOR, EXOR, $\veebar$, $\oplus$, `^`, `^^^`, `><`, etc.

| $A$ | $B$ | $A \oplus B$ |
|-----|-----|:------------:|
| F | F | F |
| F | V | V |
| V | F | V |
| V | V | F |

 - Commutatif: $A \oplus B \Leftrightarrow B \oplus A$
 - Associatif: $A \oplus (B \oplus C) \Leftrightarrow (A \oplus B) \oplus C$
 - Idempotence: $A \oplus A \Leftrightarrow 0$

\pause

$$ (A \oplus B) \oplus A \Leftrightarrow (A \oplus A) \oplus B \Leftrightarrow 0 \oplus B \Leftrightarrow B $$

# XOR en pratique

## One-time Pad

$$m \oplus s \leftrightarrow c$$
$$m \leftrightarrow c \oplus s $$


Chiffre parfait. Impossible à craquer. Si...

  1. La clé n'est pas plus courte que le message
  2. La clé est vraiment aléatoire
  3. La clé n'est jamais réutilisée
  4. La clé reste secrète
  \pause
  0. La clé est acheminée de façon sécuritaire

# À votre tour! (Fly, b\*tch!)

![Oiseaux](bird.png){ height=50% }

- [Exercices: https://cutt.ly/pL17ta2](https://gitlab.com/secuqam/crypto-100/-/blob/main/exercices.md)
- [Plus d'exercices: cryptopals.com](https://www.cryptopals.com/)

# Aide-mémoire Python 3

- `"String", b"Bytes"`
- `"allô".encode(encoding) -> bytes`
- `b"all\\xf4".decode(encoding) -> str`
- `base64.b64encode(s: bytes) -> bytes`
- `base64.b64decode(s: bytes) -> bytes`
- `binascii.hexlify(data: bytes) -> bytes`
- `binascii.unhexlify(data: bytes) -> bytes`
- `65.to_bytes(1, byteorder='little') -> bytes (b"\\x41")

# Bonus: Modes de chiffrement par bloc

![Electronic Codebook: Chiffrement](ecb_enc.png){ height=45% }
![Electronic Codebook: Dechiffrement](ecb_dec.png){ height=45% }

# Bonus: Modes de chiffrement par bloc

![Cipher block chaining: Chiffrement](cbc_enc.png){ height=45% }
![Cipher block chaining: Dechiffrement](cbc_dec.png){ height=45% }
