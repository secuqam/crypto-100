import base64
import binascii
import itertools
import random
import textwrap

from Crypto.Util.strxor import strxor, strxor_c


def extend_buffer(buffer, length):
    """Take a string or bytes object and repeat for "length" characters.
    """
    return (buffer*length)[:length]


class Challenge():
    def __init__(self, plaintext, ciphertext, key, hints, description):
        self.plaintext = plaintext
        self.ciphertext = ciphertext
        self.key = key
        self.hints = hints
        self.description = description


def gen1():
    plaintext = "Mais qu'est-ce que ça peut vous foutre ? De toute façon, c'est pas vous qui l'avez faite, si ?".encode('latin1')
    key = random.randint(0, 255)
    ciphertext = strxor_c(plaintext, key)

    ciphertext_1 = ciphertext[:len(ciphertext)//2]
    ciphertext_2 = ciphertext[len(ciphertext)//2:]

    description = "Single byte XOR with known key."

    hints = [
            ('ciphertext part 1', ciphertext_1),
            ('ciphertext part 2', ciphertext_2),
            ('key', key)
            ]

    chal = Challenge(plaintext, ciphertext, key, hints, description)

    print("Challenge 1")
    print("Plaintext:", plaintext.decode('latin1'))
    print("Ciphertext part 1:", base64.b64encode(ciphertext_1).decode())
    print("Ciphertext part 2:", binascii.hexlify(ciphertext_2).decode())
    print("Ciphertext:", binascii.hexlify(ciphertext).decode())
    print("key:", hex(key))
    print()

    return chal


def test1():
    chal = gen1()
    decrypted = strxor_c(chal.ciphertext, chal.key)
    assert decrypted == chal.plaintext


def gen2():
    plaintext = "Sans vouloir la ramener, la seule différence concrète avec des briques, c'est que vous appelez ça des tartes hein !".encode('latin1')
    key = random.randint(0, 255)
    ciphertext = strxor_c(plaintext, key)

    description = "Single byte XOR with unknown key."

    hints = [
            ('ciphertext', binascii.hexlify(ciphertext)),
            ]

    chal = Challenge(plaintext, ciphertext, key, hints, description)

    print("Challenge 2")
    print("Plaintext:", plaintext.decode('latin1'))
    print("Ciphertext:", binascii.hexlify(ciphertext).decode())
    print("key:", hex(key))
    print()

    return chal


def test2():
    chal = gen2()
    found = False

    for x in range(256):
        b = x.to_bytes(1, byteorder='big')
        decrypted = strxor(chal.ciphertext, extend_buffer(b, len(chal.ciphertext)))
        if decrypted == chal.plaintext:
            found = True
            break

    assert found


def gen3():
    plaintext = "Ah et puis attention, faut pas s'amuser à attaquer ça avec des dents de lait, hein !".encode('latin1')
    key = b"Kaamelot"
    ciphertext = strxor(plaintext, extend_buffer(key, len(plaintext)))

    description = "Repeating key XOR with known key."

    hints = [
            ('ciphertext', binascii.hexlify(ciphertext)),
            ('key', binascii.hexlify(key)),
            ]

    chal = Challenge(plaintext, ciphertext, key, hints, description)

    print("Challenge 3")
    print("Plaintext:", plaintext.decode('latin1'))
    print("Ciphertext:", binascii.hexlify(ciphertext).decode())
    print("key:", key)
    print()

    return chal


def test3():
    chal = gen3()
    decrypted = strxor(chal.ciphertext, extend_buffer(chal.key, len(chal.ciphertext)))
    assert decrypted == chal.plaintext


def gen4():
    plaintext_1 = "Ca depend des sources, mais je crois bien que c'est un Chevalier de Provence..".encode('latin1')
    plaintext_2 = "Mais, c'est le Chevalier de Provence ou le Chevalier gaulois? Faudrait savoir!".encode('latin1')
    plaintext_3 = "Le probleme, c'est que si ce type commence a vraiment faire parler de lui, il ".encode('latin1')
    plaintext_4 = "Mais arretez avec votre Chevalier gaulois ! J'vous dis que c'est des conneries".encode('latin1')
    key = "Élias de Kelliwic'h, dit « le Fourbe ». Pour ainsi dire un confrère.".encode('latin1')

    key_adjusted = extend_buffer(key, len(plaintext_1))
    ciphertext_1 = strxor(plaintext_1, key_adjusted)
    ciphertext_2 = strxor(plaintext_2, key_adjusted)
    ciphertext_3 = strxor(plaintext_3, key_adjusted)
    ciphertext_4 = strxor(plaintext_4, key_adjusted)

    description = "Many time pad"

    hints = [
            ('ciphertext_1', binascii.hexlify(ciphertext_1)),
            ('ciphertext_2', binascii.hexlify(ciphertext_2)),
            ('ciphertext_3', binascii.hexlify(ciphertext_3)),
            ('ciphertext_4', binascii.hexlify(ciphertext_4)),
            ]

    chal = Challenge([plaintext_1, plaintext_2], [ciphertext_1, ciphertext_2], key, hints, description)

    print("Challenge 4")
    print("Plaintext 1:", plaintext_1.decode('latin1'))
    print("Plaintext 2:", plaintext_2.decode('latin1'))
    print("Plaintext 3:", plaintext_3.decode('latin1'))
    print("Plaintext 4:", plaintext_4.decode('latin1'))
    print("Ciphertext 1:", binascii.hexlify(ciphertext_1).decode())
    print("Ciphertext 2:", binascii.hexlify(ciphertext_2).decode())
    print("Ciphertext 3:", binascii.hexlify(ciphertext_3).decode())
    print("Ciphertext 4:", binascii.hexlify(ciphertext_4).decode())
    print("key:", key)
    print()

    return chal


def test4():
    chal = gen4()
    p1p2 = strxor(chal.ciphertext[0], chal.ciphertext[1])
    p1 = strxor(p1p2, chal.plaintext[1])
    p2 = strxor(p1p2, chal.plaintext[0])
    key = strxor(chal.ciphertext[0], p1)

    assert p1 == chal.plaintext[0]
    assert p2 == chal.plaintext[1]
    assert key[:len(chal.key)] == chal.key


def gen5():
    with open('./scripte.txt', 'rb') as fd:
        plaintext = fd.read()

    key = b"UQAM{Enfin_un_vrai_defi_de_crapto_pepos_a_bien_pris_son_temps!1}"

    extended_key = extend_buffer(key, len(plaintext))
    ciphertext = strxor(plaintext, extended_key)

    description = "Many time pad with unknown block size"

    hints = [
            ('ciphertext_1', base64.b64encode(ciphertext)),
            ]

    chal = Challenge(plaintext, ciphertext, key, hints, description)

    printable_ciphertext = base64.b64encode(ciphertext).decode()
    printable_ciphertext = '\n'.join(textwrap.wrap(printable_ciphertext, width=80))
    print("Challenge 4")
    print("Plaintext: ",plaintext.decode('utf-8'))
    print("Ciphertext:", printable_ciphertext)
    print("key:", key)
    print()

    return chal


def main():
    test1()
    test2()
    test3()
    test4()
    gen5()


if __name__ == '__main__':
    main()
